import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/');
  }

  getMessage() {
    return element(by.css('gender strong')).getText();
  }
}
