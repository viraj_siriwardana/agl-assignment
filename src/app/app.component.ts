import { Component, OnInit, OnDestroy } from '@angular/core';

import * as _ from 'underscore';

import { PeopleService } from './people.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public people: Array<any> = [];

  constructor(private _peopleService: PeopleService) {
  }

  ngOnInit(): void {
    this._peopleService.getAllPeople()
      .subscribe(people => {
        this.people = people;
      });
  }
}
