import { Injectable } from '@angular/core';
import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { PeopleService } from './people.service';

export class FakePeopleService {
  public getAllPeople(): Observable<any[]> {
    return Observable.of(JSON.parse(`[{
      "name": "Bob",
      "gender": "Male",
      "age": 23,
      "pets": [{
        "name": "Garfield",
        "type": "Cat"
      },
      {
        "name": "Fido",
        "type": "Dog"
      }]
    },
    {
      "name": "Jennifer",
      "gender": "Female",
      "age": 18,
      "pets": [{
        "name": "Garfield",
        "type": "Cat"
      }]
    },
    {
      "name": "Steve",
      "gender": "Male",
      "age": 45,
      "pets": null
    },
    {
      "name": "Fred",
      "gender": "Male",
      "age": 40,
      "pets": [{
        "name": "Tom",
        "type": "Cat"
      },
      {
        "name": "Max",
        "type": "Cat"
      },
      {
        "name": "Sam",
        "type": "Dog"
      },
      {
        "name": "Jim",
        "type": "Cat"
      }]
    }]`));
  }
}

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      providers: [
        {provide: PeopleService, useClass: FakePeopleService}
    ]
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it(`should have a valid people array`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.people).toBeDefined();
  }));

  it('should render 4 span tags', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('gender')).toBeDefined();
  }));
});
