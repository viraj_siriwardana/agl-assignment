import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { environment } from '../environments/environment';
import { AppComponent } from './app.component';
import { Configuration } from './app.configuration';
import { ConfigurationParameters } from './app.configuration';
import { PeopleService } from './people.service';

export function createMoviesService(httpClient: HttpClient) {
  return new PeopleService(httpClient, new Configuration({
    basePath: environment.baseApiUrl,
  }));
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [{
    provide: PeopleService,
    deps: [HttpClient],
    useFactory: createMoviesService
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
