const express = require('express');
const axios = require('axios');

var _ = require('underscore');

const router = express.Router();
const BASE_API_URL = 'http://agl-developer-test.azurewebsites.net';

router.get('/people', (req, res) => {
  axios.get(`${BASE_API_URL}/people.json`)
    .then((response) => {
      var people = response.data;

      var mapped = _.chain(people)
      .groupBy('gender')
      .map(function(value, key) {
          return {
              gender: key,
              pets: _.chain([].concat.apply([], _.pluck(value, 'pets')))
                .where({type: 'Cat'})
                .sortBy((pet) => pet.name)
          }
      })
      .value();
     
      res.status(200).json(mapped);
    })
    .catch((error) => {
      res.status(500).send(error);
    });
});

module.exports = router;